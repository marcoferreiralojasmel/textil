/**
 * @description Report responsible for bring the online result of "Desafio Textil"
 * @author Marco Ferreira - <marco.ferreira@lojasmel.com>
 * @since 28/04/2018
 */
//==============================================================================
/**
 * Includes
 */
__include(-1895829540 /* Cores.ijs */);
__include( -1895818967 /* /products/Custom/library/objects/Lojasmel/TI/Access Log Screens/AcessLogScreensController.ijs */)
/**
 * Control of screen access
 */
var accessLog = new AccessLogScreensController();
accessLog.insertLog(this.key);
/**
 * Permissions settings
 */
this.permissionKeyWords.push("canSeeValues");
const canSeeSellsValues = this.hasPermission("canSeeValues");
/**
 * Title
 */
this.title = "Desafio Têxtil 23/05 a 22/07";
/**
 * Date settings
 */
const initialDate = new Date(2018,5-1,23);
const finalDate   = new Date(2018,7-1,22);
/**
 * key of HTML with the award table
 */
const awardTable  = -1895818527 /* /products/Custom/library/objects/consultas/links/2018/awardTextil.htm */;
/**
 * key of banner image
 */
const banner = -1895818526 /* textilRule */;
/*
================================================================================
    Processing Functions
================================================================================
*/
/**
 * @description Function responsible for bring the sells filtered by date, buyer, sell class
 */
this.buildsQuery = function buildsQuery(){

    let sql = "";
    sql += "SELECT p.EMISSAO, ";
    sql += "       p.ZHORAREAL as IHOUR, ";
    sql += "p.ESTABELECI, ";
    sql += "r.FABRICANTE as FABRICANTE, ";
    sql += "SUM(p.TOTAL) as TOTAL ";
    sql += "FROM PEDIDO p ";
    sql += "INNER JOIN RECURSO r ";
    sql += "ON(p.RECURSO = r.CHAVE) ";
    sql += "INNER JOIN ENTIDADE e ";
    sql += " ON(e.CHAVE = p.PESSOA) ";
    sql += queryUtilities.clauseWhere("WHERE","p.EMISSAO",">=",initialDate);
    sql += queryUtilities.clauseWhere("AND","p.EMISSAO", "<=",finalDate);
    sql += queryUtilities.clauseWhereOfKeys("AND", "r.CLASSE", connection.getChildren(-1895833776 /* Cama, Mesa & Banho */) )
    sql += queryUtilities.clauseWhereOfKeys("OR", "r.CLASSE", connection.getChildren(-1895833676 /* Decoração */) )
    sql += queryUtilities.clauseWhereOfKeys("AND","p.CLASSE",connection.getChildren(-1899999812 /* Vendas */));
    sql += queryUtilities.clauseWhereOfKeys("AND","e.CLASSE",connection.getChildren(-1895833052 /* Funcionários Ativos */), true);
    sql += "AND p.BAIXADO IS NOT NULL ";
    sql += "AND p.CANCELAMEN IS NULL ";
    sql += "AND p.CHDEVOLUC IS NULL ";
    sql += "AND p.ESTABELECI NOT IN(-1895829337 /* + Ecommerce */,-1895832986 /* 01 - Conceição 1 Estab */)"
    sql += "GROUP BY p.ESTABELECI, ";
    sql += "         p.EMISSAO, ";
    sql += "         p.PESSOA, ";
    sql += "         IHOUR, ";
    sql += "         FABRICANTE";
    return database.query(sql);
}
/**
 * @description Function responsible for bring the sells filtered by store and vendor
 * @return {DataSet}
 */
this.buildsQueryForEmployeers = function buildsQueryForEmployeers(){

    var sql = "";
    sql += "SELECT SUM(p.TOTAL) as TOTAL, ";
    sql += "       p.REPRESENTA, ";
    sql += "       p.ESTABELECI ";
    sql += "FROM PEDIDO p ";
    sql += "INNER JOIN RECURSO r ";
    sql += "ON(p.RECURSO = r.CHAVE) ";
    sql += "WHERE p.CLASSE IN ("+connection.getChildren(-1899999812 /* Vendas */)+") ";
    sql += "AND p.CANCELAMEN IS NULL ";
    sql += "AND p.CHDEVOLUC IS NULL ";
    sql += "AND p.REPRESENTA NOT IN(-1895820010 /* Vendedor Não Informado */)"
    sql += "AND p.REPRESENTA <> p.ZOPERADOR ";
    sql += queryUtilities.clauseWhere("AND","p.EMISSAO",">=",initialDate);
    sql += queryUtilities.clauseWhere("AND","p.EMISSAO", "<=",finalDate);
    sql += queryUtilities.clauseWhereOfKeys("AND", "r.CLASSE", connection.getChildren(-1895833776 /* Cama, Mesa & Banho */) )
    sql += queryUtilities.clauseWhereOfKeys("OR", "r.CLASSE", connection.getChildren(-1895833676 /* Decoração */) )
    sql += "GROUP BY p.REPRESENTA, ";
    sql += "         p.ESTABELECI ";
    return database.query(sql);
}
this.sumSellValues = function sumSellValues(dsSells){
    var dsSum = dsSells.sum("REPRESENTA;ESTABELECI","TOTAL");
    dsSum.indexFieldNames = "-TOTAL";
    return dsSum;
}

/**
 * Method responsible for build the financial sells goals
 * @method
 * @returns {DataSet}
 */
this.buildsGoal = function buildsGoal(){
    let ds = new DataSet();
    ds.createField("store","integer");
    ds.createField("goal","number");
    ds.create();
    ds.append([-1895832984 /* Loja 02 */,502162.48]);
    ds.append([-1895832989 /* Loja 03 */,293394.23]);
    ds.append([-1895832988 /* Loja 04 */,629714.63]);
    ds.append([-1895832985 /* Loja 05 */,210783.17]);
    ds.append([-1895832733 /* Loja 06 */,281092.86]);
    ds.append([-1895831351 /* Loja 07 */,189781.67]);
    ds.append([-1895831350 /* Loja 08 */,365194.33]);
    ds.append([-1895828527 /* Loja 09 */,310366.51]);
    ds.append([-1895827616 /* Loja 10 */,103530.42]);
    ds.append([-1895827540 /* Loja 11 */,307825.84]);
    ds.append([-1895826386 /* Loja 12 */,226942.21]);
    ds.append([-1895825480 /* Loja 13 */,189825.48]);
    ds.append([-1895825255 /* Loja 14 */,307641.73]);
    ds.append([-1895825249 /* Loja 15 */,183164.94]);
    ds.append([-1895825244 /* Loja 16 */,119062.71]);
    ds.append([-1895825008 /* Loja 17 */,180082.43]);
    ds.append([-1895824120 /* Loja 18 */,82337.66]);
    ds.append([-1895824119 /* Loja 19 */,170000.00]);
    ds.append([-1895823951 /* Loja 20 */,226488.52]);
    ds.append([-1895820303 /* Loja 21 */,102900.00]);
    ds.append([-1895820297 /* Loja 22 */,399000.00]);
    ds.append([-1895820291 /* Loja 23 */,220000.00]);
    ds.post();
    return ds;
}
/**
 * Functions responsible for build the final result
 * @method
 * @returns {DataSet}
 */
this.buildsResult = function buildsResult(){
    var ds = new DataSet();
    ds.createField("store","integer");
    ds.createField("valueSold","number");
    ds.createField("goal","number");
    ds.createField("percentReached","number");
    ds.create();
    var sells = this.buildsQuery();
    var dsSumSells = sells.sum("ESTABELECI","TOTAL");
    var goal = this.buildsGoal();
    for(dsSumSells.first(); !dsSumSells.eof; dsSumSells.next()){
        ds.append();
        ds.store = dsSumSells.estabeleci;
        ds.valueSold = dsSumSells.total;
    }
    ds.indexFieldNames = "STORE";
    goal.indexFieldNames = "STORE";
    for(ds.first(); !ds.eof; ds.next()){
        if(goal.find(ds.store)){
            ds.goal = goal.goal;
            ds.percentReached = Math.decimalRound(ds.valueSold/ds.goal, 4)*100;
        }
    }
    ds.post();
    ds.indexFieldNames = "-PERCENTREACHED";
    return ds;
}
this.buildsRankingByVendor = function buildsRankingByVendor(){
    return this.sumSellValues(this.buildsQueryForEmployeers());
}
/*
================================================================================
    Layout Functions
================================================================================
*/
/**
 * Function responsible for return the hexadecimal value color
 * @method
 * @param {number} valueSold
 * @param {number} goal
 * @param {integer} position
 * @returns {string}
 */
this.buildsColor = function buildsColor(valueSold, goal, position){
    let color = new Cores();
    if(valueSold > goal && position <= 3){
        return color.verde.normal;
    }else{
        return color.vermelho.normal;
    }
}
this.buildsReportByVendor = function buildsReportByVendor(dsSells){
    var sl = this.getSimpleLayout();
    sl.width = "100%";
    sl.title = "Ranking por Vendedor";
    sl.column("Posição");
    sl.column("Vendedor");
    sl.column("Loja");
    sl.column("Total");
    for(dsSells.first(); !dsSells.eof; dsSells.next()){
        sl.newRecord([]);
        sl.writeColumn(dsSells.recNo + "º");
        sl.writeColumn(dsSells.representa.nome);
        sl.writeColumn(dsSells.estabeleci.codigo);
        sl.writeColumn(dsSells.total.toString("%.2n"));
    };
   sl.end()
}
/**
 * Function responsible for build the simple layout report
 * @method
 * @param {DataSet} ds
 * @returns {void}
 */
this.buildsReport = function buildsReport(ds){
    let sl = this.getSimpleLayout();
    sl.width = "100%";
    sl.title = "Ranking por percentual atingido";
    sl.column("Posição");
    sl.column("Loja");
    sl.column("Valor Vendido");
    sl.column("Meta");
    sl.column("Percentual Atingindo");
    var progress = new Progress();
    try {
        progress.beginTask("Escrevendo relatório", ds.recordCount);
        for(ds.first(); !ds.eof; ds.next()){
            var goal = "Não Disponível";
            var sellsValue = "Sem Permissão Para Visualizar";
            sl.newRecord([]);
            sl.writeColumn(ds.recNo + "º");
            sl.writeColumn(ds.store.codigo,{ cssStyle : {color : this.buildsColor(ds.valueSold, ds.goal, ds.recNo)}});
            if(canSeeSellsValues || session.userKey.entidade.estabeleci == ds.store){
                sl.writeColumn(ds.valueSold.toString("%.2n"));
                sl.writeColumn(ds.goal.toString("%.2n"));
            }else{
                sl.writeColumn(sellsValue);
                sl.writeColumn(goal);
            }
            sl.writeColumn(ds.percentReached + "%");
            progress.worked();
        }
    } finally {
        progress.done();
        sl.end();
    }
}
/**
 * Function responsible for build the chart using Highcharts API
 * @method
 * @param {DataSet} ds
 * @returns {string}
 */
this.buildsChart = function buildsChart(ds){
    var percent = new Array(), store = new Array();
    for(ds.first(); !ds.eof; ds.next()){
        store.push(ds.store.codigo);
        percent.push(ds.percentReached);
    }
    var chart = new uwi.highcharts.Chart(
        {
            chart: {
                defaultSeriesType: 'column',
                borderWidth: 2,
                borderColor: 'brown',
                borderRadius: 20,
                plotBorderWidth: 3,
                width: 1100,
                height: 350,
                plotBackgroundColor: '#4169E1'
            },
            title: {
                text: 'Período de '+ initialDate.toString('dd/mm/yyyy') +' A ' + finalDate.toString('dd/mm/yyyy') + '',
                style: {
                    fontSize: '20px'
                }
            },
            xAxis: {
                categories: store
            },
            tooltip : {
                valueSuffix : '%'
            },
            yAxis: {
                title: {
                    text: 'Percentual Vendido'
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: 'black'
                    },
                    columnBackgroundImage: '-1895823760'
                },
                series: {
                    duration: 2000,
                    easing: 'easeOutBounce',
                    color: '#FF8C00'
                }

            },
           series: [{
                name: 'Lojas',
                data: percent
           }]

        }
    )
    return chart;
}
/*
================================================================================
    Activity/Interaction
================================================================================
*/
this.activity("run",function(){
    this.finalResult = this.buildsResult();
});
this.interaction("writeLayout",function(){
    this.write('<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>')
    this.write('<script src="https://code.highcharts.com/highcharts.js"></script>');
    this.write("<center><h1>Regras</h1></center>");
    this.write("<center><img src = "+banner+"></center>");
    this.write("<center>");
    this.write(this.buildsChart(this.finalResult));
    this.write("</center>");
    this.write("<center>"+awardTable.icontent+"</center>");
    this.buildsReport(this.finalResult);
    this.buildsReportByVendor(this.buildsRankingByVendor())
});
